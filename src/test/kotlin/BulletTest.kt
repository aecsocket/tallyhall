import com.jme3.bullet.PhysicsSpace
import com.jme3.bullet.collision.PhysicsCollisionObject
import com.jme3.bullet.collision.shapes.PlaneCollisionShape
import com.jme3.bullet.collision.shapes.SphereCollisionShape
import com.jme3.bullet.joints.New6Dof
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.math.Plane
import com.jme3.math.Vector3f
import com.jme3.system.NativeLibraryLoader
import com.simsilica.mathd.Vec3d
import java.io.File
import java.util.logging.Level
import kotlin.test.Test

class BulletTest {
    init {
        NativeLibraryLoader.loadLibbulletjme(true, File("."), "Release", "DpMt")

        // disable the stupid logging
        PhysicsRigidBody.logger2.level = Level.WARNING
        New6Dof.logger2.level = Level.WARNING
    }

    fun createSpace(): PhysicsSpace {
        val space = PhysicsSpace(PhysicsSpace.BroadphaseType.DBVT)
        space.setGravity(Vector3f(0f, -9.81f, 0f))
        return space
    }

    fun createGroundPlane(): PhysicsRigidBody {
        val shape = PlaneCollisionShape(Plane(Vector3f.UNIT_Y, 0f))
        val body = PhysicsRigidBody(shape, PhysicsRigidBody.massForStatic)
        return body
    }

    fun createSphere(x: Double, y: Double, z: Double): PhysicsRigidBody {
        val shape = SphereCollisionShape(0.5f)
        val body = PhysicsRigidBody(shape, 1f)
        body.setPhysicsLocationDp(Vec3d(x, y, z))
        return body
    }

    fun simulate(space: PhysicsSpace, duration: Float) {
        val step = 1/60f
        repeat ((duration / step).toInt()) {
            space.update(step)
        }
    }

    fun simulateAndRelease(space: PhysicsSpace, duration: Float, bodies: Collection<PhysicsCollisionObject>): Double {
        val start = System.nanoTime()

        println(" - Adding ${bodies.size} bodies")
        bodies.forEach { space.addCollisionObject(it) }
        println(" - Simulating for $duration sec")
        simulate(space, duration)
        println(" - Done")

        val end = System.nanoTime()
        val time = (end - start) / 1.0e9
        space.destroy()

        return time
    }

    @Test
    fun testSphere() {
        val space = createSpace()
        val ground = createGroundPlane()
        val sphere = createSphere(0.0, 5.0, 0.0)
        simulateAndRelease(space, 30f, listOf(ground, sphere))
    }

    fun testNSpheres(n: Int, duration: Float): Double {
        val space = createSpace()
        val ground = createGroundPlane()

        val spheres = ArrayList<PhysicsRigidBody>()
        repeat(n) {
            spheres.add(createSphere(
                randomDouble(-50.0, 50.0),
                randomDouble(25.0, 75.0),
                randomDouble(-50.0, 50.0)
            ))
        }

        return simulateAndRelease(space, duration, listOf(ground) + spheres)
    }
}
