import phystest.*
import physx.PxTopLevelFunctions.*
import physx.common.PxCpuDispatcher
import physx.common.PxDefaultAllocator
import physx.common.PxDefaultErrorCallback
import physx.common.PxTolerancesScale
import physx.geometry.PxGeometry
import physx.physics.*
import kotlin.math.sqrt
import kotlin.test.Test

class PhysXTest {
    val numThreads = Runtime.getRuntime().availableProcessors()

    val tolerances: PxTolerancesScale
    val physics: PxPhysics
    val stdDispatcher: PxCpuDispatcher
    val stdMaterial: PxMaterial
    val stdFilterData: PxFilterData

    init {
        val version = getPHYSICS_VERSION()
        val allocator = PxDefaultAllocator()
        val errorCb = PxDefaultErrorCallback()
        val foundation = CreateFoundation(version, allocator, errorCb)

        tolerances = PxTolerancesScale()
        physics = CreatePhysics(version, foundation, tolerances)

        stdDispatcher = DefaultCpuDispatcherCreate(numThreads)
        stdMaterial = physics.createMaterial(0.5f, 0.5f, 0.5f)
        stdFilterData = PxFilterData(
            1,    // collision group: 0 (i.e. 1 << 0)
            -0x1, // collision mask: collide with everything
            0,    // no additional collision flags
            0     // unused
        )
    }

    fun createSpace(): PxScene {
        val space: PxScene
        useMemory {
            val sceneDesc = pxSceneDesc(tolerances)
            sceneDesc.gravity = pxVec3(0f, -9.81f, 0f)
            sceneDesc.cpuDispatcher = stdDispatcher
            sceneDesc.filterShader = DefaultFilterShader()
            space = physics.createScene(sceneDesc)
        }
        return space
    }

    fun createStaticBody(shape: PxShape, x: Float, y: Float, z: Float): PxRigidStatic {
        val body = useMemory {
            physics.createRigidStatic(pxTransform(pxVec3(x, y, z), pxQuat()))
        }
        body.attachShape(shape)
        return body
    }

    fun createStaticBody(geom: PxGeometry, x: Float, y: Float, z: Float): PxRigidStatic {
        val shape = physics.createShape(geom, stdMaterial, true)
        shape.simulationFilterData = stdFilterData
        return createStaticBody(shape, x, y, z)
    }

    fun createSphere(x: Float, y: Float, z: Float): PxRigidDynamic {
        val shape: PxShape
        val body: PxRigidDynamic
        useMemory {
            val geom = pxSphereGeometry(0.5f)
            shape = physics.createShape(geom, stdMaterial, true)
            body = physics.createRigidDynamic(pxTransform(pxVec3(x, y, z), pxQuat()))
        }
        shape.simulationFilterData = stdFilterData
        body.mass = 1f
        body.attachShape(shape)
        return body
    }

    fun createGroundPlane(): PxRigidStatic {
        val shape: PxShape
        return useMemory {
            val geom = pxPlaneGeometry()
            shape = physics.createShape(geom, stdMaterial, true)

            val r = 1f / sqrt(2f)
            shape.localPose = pxTransform(pxVec3(), pxQuat(0f, 0f, r, r))
            createStaticBody(shape, 0f, 0f, 0f)
        }
    }

    fun simulate(scene: PxScene, duration: Float) {
        val step = 1/60f
        repeat ((duration / step).toInt()) {
            scene.simulate(step)
            scene.fetchResults(true)
        }
    }

    fun simulateAndRelease(space: PxScene, duration: Float, bodies: Collection<PxActor>): Double {
        val start = System.nanoTime()

        println(" - Adding ${bodies.size} bodies")
        bodies.forEach { space.addActor(it) }
        println(" - Simulating for $duration sec")
        simulate(space, duration)
        println(" - Done")

        val end = System.nanoTime()
        val time = (end - start) / 1.0e9
        space.release()
        bodies.forEach { it.release() }

        return time
    }

    @Test
    fun testSphere() {
        val space = createSpace()
        val ground = createGroundPlane()
        val sphere = createSphere(0f, 5f, 0f)
        simulateAndRelease(space, 30f, listOf(ground, sphere))
    }

    fun testNSpheres(n: Int, duration: Float): Double {
        val space = createSpace()
        val ground = createGroundPlane()

        val spheres = ArrayList<PxRigidDynamic>()
        repeat(n) {
            spheres.add(createSphere(
                randomFloat(-50f, 50f),
                randomFloat(25f, 75f),
                randomFloat(-50f, 50f)
            ))
        }

        return simulateAndRelease(space, duration, listOf(ground) + spheres)
    }

    @Test
    fun testSpheres1() {
        val start = System.nanoTime()
        testNSpheres(100_000, 5f)
        val end = System.nanoTime()
        println("Time = ${(end - start) / 1.0e9}")
    }
}
