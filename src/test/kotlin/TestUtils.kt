import kotlin.random.Random

fun randomDouble(from: Double, to: Double) = Random.nextDouble() * (to - from) - from
fun randomFloat(from: Float, to: Float) = Random.nextFloat() * (to - from) - from
