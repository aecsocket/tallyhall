import org.junit.jupiter.api.Test

class ComparisonTest {
    data class TestSettings(
        val count: Int,
        val duration: Float
    )

    @Test
    fun testFull() {
        val settings = listOf(
            TestSettings(1_000, 5f),
            TestSettings(1_000, 15f),
            TestSettings(1_000, 30f),

            TestSettings(5_000, 5f),
            TestSettings(5_000, 15f),
            TestSettings(5_000, 30f),

            TestSettings(15_000, 5f),
            TestSettings(15_000, 15f),
            TestSettings(15_000, 30f),

            TestSettings(100_000, 5f),
            TestSettings(100_000, 15f),
            TestSettings(100_000, 30f)
        )

        data class Result(
            val settings: TestSettings,
            val bullet: Double,
            val physx: Double
        )

        val doBullet = false
        val doPhysx = true

        val bulletTest = BulletTest()
        val physxTest = PhysXTest()
        val results = ArrayList<Result>()
        settings.forEach { setting ->
            val (count, duration) = setting
            println("$count spheres / $duration sec:")
            val bullet = if (doBullet) bulletTest.testNSpheres(count, duration) else 1.0
            println("  Bullet: $bullet sec")
            val physx = if (doPhysx) physxTest.testNSpheres(count, duration) else 1.0
            println("  PhysX: $physx sec")

            results.add(Result(setting, bullet, physx))
        }

        println("# Bodies | Duration | Bullet     | PhysX      | Bullet / PhysX")
        println("---------+----------+----------+----------+---------------")
        results.forEach { (setting, bullet, physx) ->
            val (count, duration) = setting
            val ratio = bullet / physx
            println(" %7d | %6.3f s | %8.3f s | %8.3f s | %13.2f".format(count, duration, bullet, physx, ratio))
        }
    }
}
