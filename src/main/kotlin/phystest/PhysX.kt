package phystest

import org.lwjgl.system.MemoryStack
import physx.common.*
import physx.geometry.PxBoxGeometry
import physx.geometry.PxPlaneGeometry
import physx.geometry.PxSphereGeometry
import physx.physics.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
fun <R> useMemory(block: MemoryStack.() -> R): R {
    contract {
        callsInPlace(block, InvocationKind.EXACTLY_ONCE)
    }

    return MemoryStack.stackPush().use(block)
}

object ShapeFlag {
    val SIMULATION_SHAPE = PxShapeFlagEnum.eSIMULATION_SHAPE.value
    val SCENE_QUERY_SHAPE = PxShapeFlagEnum.eSCENE_QUERY_SHAPE.value
    val TRIGGER_SHAPE = PxShapeFlagEnum.eTRIGGER_SHAPE.value
    val VISUALIZATION = PxShapeFlagEnum.eVISUALIZATION.value
}

val PxIdentity get() = PxIDENTITYEnum.PxIdentity

fun MemoryStack.pxVec3(x: Float, y: Float, z: Float) =
    PxVec3.createAt(this, MemoryStack::nmalloc, x, y, z)
fun MemoryStack.pxVec3() =
    PxVec3.createAt(this, MemoryStack::nmalloc, 0f, 0f, 0f)
fun MemoryStack.pxQuat(x: Float, y: Float, z: Float, w: Float) =
    PxQuat.createAt(this, MemoryStack::nmalloc, x, y, z, w)
fun MemoryStack.pxQuat() =
    PxQuat.createAt(this, MemoryStack::nmalloc, PxIdentity)
fun MemoryStack.pxTransform(vec: PxVec3, quat: PxQuat) =
    PxTransform.createAt(this, MemoryStack::nmalloc, vec, quat)
fun MemoryStack.pxTransform() =
    PxTransform.createAt(this, MemoryStack::nmalloc, PxIdentity)
fun MemoryStack.pxSphereGeometry(ir: Float) =
    PxSphereGeometry.createAt(this, MemoryStack::nmalloc, ir)
fun MemoryStack.pxBoxGeometry(hx: Float, hy: Float, hz: Float) =
    PxBoxGeometry.createAt(this, MemoryStack::nmalloc, hx, hy, hz)
fun MemoryStack.pxPlaneGeometry() =
    PxPlaneGeometry.createAt(this, MemoryStack::nmalloc)
fun MemoryStack.pxShapeFlags(flags: Byte) =
    PxShapeFlags.createAt(this, MemoryStack::nmalloc, flags)
fun MemoryStack.pxShapeFlags(flags: Int) =
    PxShapeFlags.createAt(this, MemoryStack::nmalloc, flags.toByte())
fun MemoryStack.pxFilterData(w0: Int, w1: Int, w2: Int, w3: Int) =
    PxFilterData.createAt(this, MemoryStack::nmalloc, w0, w1, w2, w3)
fun MemoryStack.pxSceneDesc(tolerances: PxTolerancesScale) =
    PxSceneDesc.createAt(this, MemoryStack::nmalloc, tolerances)
