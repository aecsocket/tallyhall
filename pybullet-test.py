import pybullet as pb
import time
import pybullet_data

step = 1/60.0
count = 1000
duration = 1.0

client = pb.connect(pb.DIRECT)
pb.setGravity(0.0, 0.0, -9.81)
pb.setTimeStep(step)

planeShape = pb.createCollisionShape(pb.GEOM_PLANE, planeNormal=[0.0, 0.0, 1.0])
plane = pb.createMultiBody(baseMass=0, basePosition=[0.0, 0.0, 0.0])

spheres = []
sphereShape = pb.createCollisionShape(pb.GEOM_SPHERE, radius=0.5)
print(f"Adding {count} bodies")
start = time.time_ns()
for i in range(count):
    sphere = pb.createMultiBody(baseMass=1, basePosition=[0.0, 5.0, 0.0])
    spheres.append(sphere)
end = time.time_ns()
print(f"Done in {(end - start) / 1.0e9} sec")

print(f"Simulating for {duration} sec")
start = time.time_ns()
mi = int(duration / step)
for i in range(int(duration / step)):
    a = time.time_ns()
    pb.stepSimulation()
    b = time.time_ns()
    print(f"[{i} / {mi}] {(b - a) / 1.0e9} sec")
end = time.time_ns()
print(f"Done in {(end - start) / 1.0e9} sec")
